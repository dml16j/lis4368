# LIS4368- Advanced Web Applications

## Duncan Lopatine

### Project 1 Requirements:
* Create a basic bootstrap client-side validation
* Modify Meta tags
* Add jQuery validation and regular expressions
* Edit code to make sure client-side validation works
* Canvas Links: Bitbucket Repo
* http://localhost:9999/lis4368/p1/index.jsp
* Screenshots of Home Screen, Passed Validation, and Failed Validation


#### Project Screenshots:

*Screenshot of LIS4368 Portal:*
![LIS4368 Portal Screenshot](img/project1.png "P1 Portal Screenshot") 

*P1 Failed Validation:*
![P1 Failed Validation Screenshot](img/p1img2.png "P1 Failed Validation")

*P1 Passed Validation:*   
![P1 Passed Validation Screenshot](img/p1img3.png "P1 Passed Validation")