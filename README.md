# LIS 4368- Advanced Web Development
 
## Duncan Lopatine
 
### LIS4368 Requirements:

*Course Work Links* 

1. [A1 README.md](a1/README.md "My A1 README.md file")
	
    - Install JDK
    - Install TOMCAT
    - Screenshots of Installation
    
    
    
2. [A2 README.md](a2/README.md "My A2 README.md file")
	
    - Develop and deploy webapp
    - Write a welcome page
    - Write/compile servlet
    - Write Database servlet

3. [A3 README.md](a3/README.md "My A3 README.md file")
	
    - Create Entity Relationship Diagram
    - Include at least 10 records for each table)
    - Provide Bitbucket Read-only access to Repo
    - SQL Statements
    - Include img & doc folder w/ A3 as a .mwb, .sql, and .png
    


4. [P1 README.md](p1/README.md "My P1 README.md file")
	
    - Create a basic bootstrap client-side validation
    - Modify Meta tags
    - Add jQuery validation and regular expressions
    - Edit code to make sure client-side validation works



5. [A4 README.md](a4/README.md "My A4 README.md file")
	
    - Edit and compile CustomerServlet.java and Customer.java
    - Include screenshots of A4 Failed and Passed Validation
    - Canvas Link of Bitbucket Repo



6. [A5 README.md](a5/README.md "My A5 README.md file")

    - Show data that was entered in database
    - Edit A5 webpage
    - Include screenshots of A5 User Form, Passed Validation, and active database entry
    - Canvas link of Bitbucket repo