# LIS4368- Advanced Web Applications

## Duncan Lopatine

### Assignment 3 Requirements:
* Create Entity Relationship Diagram
* Include at least 10 records for each table)
* Provide Bitbucket Read-only access to Repo
* SQL Statements
* Include img & doc folder w/ A3 as a .mwb, .sql, and .png

#### Assignment Screenshots:

*Screenshot of A3 ER Diagram*:

![A3 ER Diagram Screenshot](img/a3.png)

*Screenshot of running HelloHome*:http://localhost:9999/hello/HelloHome.html

![HelloHome Screenshot](img/a2img1.png)

*Screenshot of running SayHello*:http://localhost:9999/hello/sayhello

![SayHello Screenshot](img/a2img2.png)

*Screenshot of running Querybook*:http://localhost:9999/hello/querybook.html

![Querybook Screenshot](img/a2img3.png)

*Screenshot of running Querybook Results*:http://localhost:9999/hello/querybook.html

![Querybook Results Screenshot](img/a2img4.png)