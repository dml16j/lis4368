# LIS4368- Advanced Web Applications

## Duncan Lopatine

### Assignment 2 Requirements:
* Develop and Deploy Webapp
* Write a welcome page
* Write/Compile Servlet
* Write a Database Servlet

#### Assignment Screenshots:

*Screenshot of running directory*

![Directory Screenshot](img/a2img.png)

*Screenshot of running HelloHome*:

![HelloHome Screenshot](img/a2img1.png)

*Screenshot of running SayHello*:

![SayHello Screenshot](img/a2img2.png)

*Screenshot of running Querybook*:

![Querybook Screenshot](img/a2img3.png)

*Screenshot of running Querybook Results*:

![Querybook Results Screenshot](img/a2img4.png)