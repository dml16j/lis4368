# LIS4368- Advanced Web Applications

## Duncan Lopatine

### Assignment 5 Requirements:
* Create Entity Relationship Diagram
* Include at least 10 records for each table)
* Provide Bitbucket Read-only access to Repo
* SQL Statements
* Include img & doc folder w/ A3 as a .mwb, .sql, and .png

#### Assignment Screenshots:

*Screenshot of A5 Valid User Form Entry*:

![A5 Screenshot of Valid User Form Entry](img/a5img1.png)

*Screenshot of A5 Passed Validation*:

![A5 Screenshot of Passed Validation](img/a5img2.png)

*Screenshot of A5 Associated Database Entry*:

![A5 Screenshot of Associated Database Entry](img/a5img3.png)



