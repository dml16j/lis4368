# LIS4368- Advanced Web Applications

## Duncan Lopatine

### Assignment 4 Requirements:
* Edit and compile CustomerServlet.java and Customer.java
* Include screenshots of A4 Failed and Passed Validation
* Canvas Link of Bitbucket Repo

#### Assignment Screenshots:

*Screenshot of A4 Failed Validation*:

![A4 Failed Validation Screenshot](img/a4img1.png)

*Screenshot of A4 Passed*:

![A4 Failed Validation Screenshot](img/a4img2.png)
