# LIS4368- Advanced Web Applications

## Duncan Lopatine

### Assignment 3 Requirements:
* Create Entity Relationship Diagram
* Include at least 10 records for each table)
* Provide Bitbucket Read-only access to Repo
* SQL Statements
* Include img & doc folder w/ A3 as a .mwb, .sql, and .png

#### Assignment Screenshots:

*Screenshot of A3 EER Diagram*:

![A3 EER Diagram Screenshot](img/a3.png)

*A3 Docs: a3.mwb and a3.sql:*

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")   
[A3 SQL File](docs/a3.sql "A3 SQL Script")

