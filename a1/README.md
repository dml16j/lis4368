
# LIS4368- Advanced Web Applications

## Duncan Lopatine

### Assignment 1 Requirements:
* Install JDK
* Install Tomcat
* Create Repo
* Answer Questions
### Git commands w/short descriptions:
1. git init- initialize a git repository
2. git status- let's you see which files git knows exist
3. git add - adds a file to the staging environment
4. git commit - creates your first commit
5. git push - pushes the commit to repo
6. git pull - pulls commit from repo
7. git checkout master- switches branches back to master branch

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost:9999/*:

![AMPPS Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/HelloWorld.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/dml16j/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Link to running environment*
[Link to website](http://localhost:9999/lis4368/ "Website")
